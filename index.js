var cheeseStickError = require('./cheeseStickError.js');

module.exports = cheeseStickConsumption;

function cheeseStickConsumption(n) {
  try {
    if (!Number.isInteger(n | 0) || n < 0) {
      throw new cheeseStickError('Argument must be a positive integer');
    }
    return (n | 0) * ((n / 5 | 0) + 2);
  } catch (error) {
    console.error(error.stack);
    return -1;
  }
}


