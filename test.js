var test = require('tape');
var cheeseStickError = require('./cheeseStickError.js');
var cheeseStickConsumption = require('./index.js');


test('cheeseStickConsumption', function (assert) {
  assert.plan(7);
  assert.equal(typeof cheeseStickConsumption, 'function');
  assert.equal(cheeseStickConsumption(3), 6);
  assert.equal(cheeseStickConsumption(5), 15);
  assert.equal(cheeseStickConsumption(17), 85);
  assert.equal(cheeseStickConsumption(0), 0);
  assert.equal(cheeseStickConsumption(-1), -1);
  assert.equal(cheeseStickConsumption(-8), -1);
})
