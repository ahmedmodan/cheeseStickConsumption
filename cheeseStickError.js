require('util').inherits(cheeseStickError, Error);

module.exports = cheeseStickError;

function cheeseStickError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message =  message;
};
