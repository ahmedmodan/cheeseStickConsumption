# Cheese Stick Consumption

This is a module to help you calculate cheese stick consumption of current MKS fellows.

example usage:
```javascript
  var cheeseStickConsumption = require('cheese-stick-consumption');

  cheeseStickConsumption(5);

```

